package at.ac.tuwien.inso.sqm.controller.student;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import at.ac.tuwien.inso.sqm.entity.Grade;
import at.ac.tuwien.inso.sqm.service.GradeServiceImpl;

@Controller
@RequestMapping("/student/grades")
public class StudentGradesController {

    @Autowired
    private GradeServiceImpl gradeService;

    @ModelAttribute("grades")
    private List<Grade> getGrades() {
        return gradeService.getGradesForLoggedInStudent();
    }

    @GetMapping
    public String getPage() {
        return "student/grades";
    }

}
