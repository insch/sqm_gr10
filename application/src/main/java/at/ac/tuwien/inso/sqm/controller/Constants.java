package at.ac.tuwien.inso.sqm.controller;

public final class Constants {

    private Constants() {
    }

    public static final int MAX_PAGE_SIZE = 10;
}
