package at.ac.tuwien.inso.sqm.utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public final class IterableUtils {

    private IterableUtils(){}

    //TODO reuse this!
    public static <T> List<T> toList(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
    }
}
